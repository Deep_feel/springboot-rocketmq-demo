package com.test.rocketmq.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: huangkunming
 * @Date: 2019/7/11 14:41
 */
@Data
public class OrderPaidEvent implements Serializable {

    private static final long serialVersionUID = -8983677932582480532L;

    private String orderNo;

    private BigDecimal paidMoney;

    private Integer orderly;
}
