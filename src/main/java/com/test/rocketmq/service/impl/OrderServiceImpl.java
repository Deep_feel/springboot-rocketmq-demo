package com.test.rocketmq.service.impl;

import com.test.rocketmq.dao.MqOrderMapper;
import com.test.rocketmq.domain.MqOrder;
import com.test.rocketmq.domain.OrderPaidEvent;
import com.test.rocketmq.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: huangkunming
 * @Date: 2021/5/25 18:38
 * @Description:
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private MqOrderMapper mqOrderMapper;


    @Override
    public Boolean createOrder(OrderPaidEvent orderPaidEvent) {
        MqOrder mqOrder = new MqOrder().setOrderNo(orderPaidEvent.getOrderNo()).setPaidMoney(orderPaidEvent.getPaidMoney());
        int i = mqOrderMapper.insertSelective(mqOrder);
        return i > 0;
    }

    @Override
    public Boolean checkOrder(String orderNo) {
        MqOrder mqOrder = mqOrderMapper.selectOne(new MqOrder().setOrderNo(orderNo));
        return mqOrder != null;
    }
}
