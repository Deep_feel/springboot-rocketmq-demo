package com.test.rocketmq.service;

import com.test.rocketmq.domain.OrderPaidEvent;

/**
 * @Author: huangkunming
 * @Date: 2021/5/25 16:39
 * @Description:
 */
public interface OrderService {

    Boolean createOrder(OrderPaidEvent orderPaidEvent);

    Boolean checkOrder(String orderNo);
}
