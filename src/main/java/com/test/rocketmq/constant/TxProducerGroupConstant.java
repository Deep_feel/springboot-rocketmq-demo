package com.test.rocketmq.constant;

/**
 * @Author: huangkunming
 * @Date: 2021/5/25 11:55
 * @Description:
 */
public class TxProducerGroupConstant {

    public static final String ORDER_TX_PRODUCER_GROUP = "order-tx-producerGroup";
}
