package com.test.rocketmq.consumer;

import com.test.rocketmq.domain.OrderPaidEventTx;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * @Author: huangkunming
 * @Date: 2019/7/12 10:48
 */
@Slf4j
@Service
@RocketMQMessageListener(consumeMode = ConsumeMode.CONCURRENTLY, topic = "tx-topic",
        consumerGroup = "tx-consumer-group", messageModel = MessageModel.CLUSTERING)
public class TxMQConsumer implements RocketMQListener<OrderPaidEventTx> {

    @Override
    public void onMessage(OrderPaidEventTx message) {
        log.info("[CollectionUpdateConsumer onMessage][消息ID:{} 消息内容：{}]", message.getOrderId(), message.toString());
        // todo
        log.info("[CollectionUpdateConsumer onMessage][消息ID:{} 消费结束]", message.getOrderId());
    }
}
