package com.test.rocketmq.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_mq_order")
@Data
@Accessors(chain = true)
public class MqOrder implements Serializable {

    private static final long serialVersionUID = -13023099790476886L;

    /**
     * 自增ID
     **/
    @Id
    private Long id;
    @Column(name = "order_no")
    private String orderNo;
    @Column(name = "paid_money")
    private BigDecimal paidMoney;

}