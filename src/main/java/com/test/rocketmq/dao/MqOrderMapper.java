package com.test.rocketmq.dao;

import com.test.rocketmq.domain.MqOrder;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface MqOrderMapper extends Mapper<MqOrder> {

}